package com.gmail.dmytro;

public class MyThread implements Runnable {
	private int[] arr;
	private int index;

	MyThread(int[] arr, int index) {
		this.arr = arr;
		this.index = index;
	}

	public void run() {
		int sum = 0;
		for (int i = 0; i < arr.length; i++) {
			if (arr[i] % index == 0) {
				sum += arr[i];
			}
		}
		System.out.println("����� ���������� ������� " + index + " = " + sum);
	}

}
